var premiseNumber = context.getVariable("request.queryparam.premiseNumber");
var servicePointId = context.getVariable("request.queryparam.servicePointId");
var deviceUtilId = context.getVariable("request.queryparam.deviceUtilId");
var isValidRequest = premiseNumber || servicePointId || deviceUtilId;   // must have 1 of the parameters
context.setVariable("isValidRequest", !!isValidRequest);
context.setVariable("invalidParam", "false");

if(!isValidRequest){
    context.setVariable("errorMessage", "Please provide a premiseNumber, servicePointId, xor deviceUtilId");
}

//Regular Expressions
var premiseNumberRegExp = /^[0-9]{4,}$/;
// var minLenRegex = new RegExp("^[0-9]{4,}$");
var idRegex = new RegExp(/^([0-9]+)_([EGeg])_[0-9]$/);
var deviceIdRegex = new RegExp("^[A-Z0-9]{12,}$");

if (premiseNumber !== null && premiseNumber !== "") {
    var premiseNumbers = premiseNumber.split(",");

    (function validatePremiseNumber() {
        
        for( var id = 0; id<premiseNumbers.length; id++) {
            
            if (!premiseNumberRegExp.test(premiseNumbers[id])) {
                isValidRequest = false;
                context.setVariable("isValidRequest", "false");
                context.setVariable("errorMessage", "Invalid deviceUtilId: [ " + premiseNumbers[id] + " ] Please provide a valid premiseNumber (numeric value and minimum length should be 4)");
                context.setVariable("paramName", "premiseNumber");
                
                return "false";
                
            }
            // else if ( !minLenRegex.test(premiseNumbers[id]) ) {
            //     isValidRequest = false;
            //     context.setVariable("isValidRequest", "false");
            //     context.setVariable("errorMessage", "String is too short (" + premiseNumbers[id].length + " chars), minimum 4");
            //     context.setVariable("paramName", "premiseNumber");
                
            //     return "false";
            // }
        }
    }())
    
}

if (servicePointId !== null && servicePointId !== "" && isValidRequest === true) {
    var servicePointIds = servicePointId.split(",");

    for( var id=0; id<servicePointIds.length; id++) {
            
        if (!idRegex.test(servicePointIds[id])) {
            isValidRequest = false;
            context.setVariable("isValidRequest", false);
            context.setVariable("errorMessage", "Invalid Service Point ID: [ " + servicePointIds[id] + " ] Please provide an appropriate service point ID. Service Point ID must be in the format:  ###########_A_#");
            context.setVariable("paramName", "servicePointId");
            
            break;
        }
    }
    
} 

if (deviceUtilId !== null && deviceUtilId !== "" && isValidRequest === true) {
    var deviceUtilIds = deviceUtilId.split(",");
    
    for( var id=0; id< deviceUtilIds.length; id++) {
            
        if (!deviceIdRegex.test(deviceUtilIds[id])) {
                
            context.setVariable("isValidRequest", false);
            context.setVariable("errorMessage", "Invalid deviceUtilId: [ " + deviceUtilIds[id] + " ] Please provide an appropriate deviceUtilId. (alphanumeric value and minimum length of 4)");
            context.setVariable("paramName", "deviceUtilId");
            break;
        }
    }
}
